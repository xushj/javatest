package com.mzz.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件控制层
 *
 * @author xushijian
 * @date 2021/06/17 17:05
 */
@RestController
@RequestMapping("file")
public class FileController {

    @PostMapping("/upload")
    public String upload(MultipartFile file) {

        //TODO 处理文件

        return "sucess";
    }
}