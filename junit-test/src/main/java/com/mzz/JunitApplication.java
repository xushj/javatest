package com.mzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Junit测试
 *
 * @author xushijian
 * @date 2021/06/17 16:09
 */
@SpringBootApplication
public class JunitApplication {

    public static void main(String[] args) {

        SpringApplication.run(JunitApplication.class);
    }
}