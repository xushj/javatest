package com.mzz

import io.gatling.core.scenario.Simulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

/**
  * Despcribe:
  *
  * （1）Scala 文件是以.scala结尾的
  * （2）注意请求参数的模拟
  *
  *  (3) 参考地址： http://www.spring4all.com/article/584
  * Created by xushijian 
  * Date: 18/2/2
  * Time: 下午6:50
  */
class ApiGatlingSimulationTest extends Simulation {


  val scn = scenario("AddAndFindPersons").repeat(100, "n") {
    exec(
      http("AddPerson-API")
        .post("http://localhost:8080/account/save")
        .header("Content-Type", "application/json")
        .body(StringBody("""{"name":"John ${n}","money":${n}}""")) // 特别注意
        .check(status.is(200))
    ).pause(Duration.apply(5, TimeUnit.MILLISECONDS))
  }
    .repeat(100, "n") {
    exec(
      http("GetPerson-API")
        .get("http://localhost:8080/account/${n}")
        .check(status.is(200)))
  }

  //设置跑多久时间
  setUp(scn.inject(atOnceUsers(30))).maxDuration(FiniteDuration.apply(3, "minutes"))
}