package com.mzz.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 用户服务
 *
 * @author xushijian
 *
 */
@Entity
public class Account {

	@Id // 注意Id 注解的引用
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;

	private double money;

	public int getId() {
		return id;
	}

	public void setId(
			int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(
			String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(
			double money) {
		this.money = money;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Account{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", money=").append(money);
		sb.append('}');
		return sb.toString();
	}
}
