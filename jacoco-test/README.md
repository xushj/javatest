
## [Jacoco 简介](https://www.jacoco.org/jacoco/trunk/doc/index.html)

```
包括基本概念、原理、如何使用、集成、开发
```

## Ant 生成报表

### 操作步骤

#### 1.安装Ant

安装Ant,并设置环境变量

#### 2.安装Jacoco


#### 3.通过远程代理的方式测试

（1） [单体服务基于Tomcat的](https://www.jianshu.com/p/8713afb89a6c)

（2） 基于SpringBoot 内置容器的
    
（3） Docker 容器中
 ```dockerfile
FROM xxxx
ENV JOB app.jar
# Step1: add jacoco
ENV JACOCO jacocoagent.jar
ENV PORT 9000
ENV ACTIVE prod
ENV LOG_PREFIX=sdma-server LOG_HOME=/data/logs/sdma-server
COPY ./target/*.jar /data/$JOB
# Step2: put jacocoagent into lib and  copy jacocoagent to /data
COPY ./lib/*.jar /data/$JACOCO
RUN yum install crontabs -y  && echo '0 4 * * * /usr/bin/pkill java' >>/var/spool/cron/root
ADD ./run.sh /data
RUN chmod a+x /data/run.sh
WORKDIR /data
ENTRYPOINT sh /data/run.sh
```

```bash
echo "export LANG=en_US.UTF-8" >> /etc/profile
source /etc/profile
if [  "$ACTIVE" ]; then
  export JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=$ACTIVE"
else
  export JAVA_OPTS="$JAVA_OPTS -Dspring.profiles.active=test"
fi

mkdir -p /logs/$LOG_PREFIX/`hostname` /data/logs
ln -s /logs/$LOG_PREFIX/`hostname` $LOG_HOME
mkdir /data/excel

JAVA_OPTS="$JAVA_OPTS -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -verbose:gc -Xloggc:$LOG_HOME/gc.log "

# Step3 : add jacoco agent setting
JAVA_OPTS="$JAVA_OPTS -javaagent:/data/jacocoagent.jar=includes=*,output=tcpserver,address=0.0.0.0,port=6300,append=true "

echo -e "Starting the Server ...\c"
#mkdir -p $LOG_HOME
echo "" >>$LOG_HOME/gc.log
/usr/sbin/crond on
java $JAVA_OPTS -jar /data/$JOB --server.port=$PORT
```
 
##### <1. 需要将jacocoagent.jar 拷贝到项目 jar 目录

##### <2. jar 执行设置  egs:Step3 

##### <3. build.xml 配置，如下：
```xml
<?xml version="1.0" ?>
<project name="jacoco" xmlns:jacoco="antlib:org.jacoco.ant" default="jacoco">
    <!--Jacoco的安装路径-->
  <property name="jacocoantPath" value="C:/Program Files/jacoco-0.8.3/lib/jacocoant.jar"/>
  <!--最终生成.exec文件的路径，Jacoco就是根据这个文件生成最终的报告的-->
  <property name="jacocoexecPath" value="c:/test-resource/jacoco.exec"/>
    <!--生成覆盖率报告的路径-->
  <property name="reportfolderPath" value="storageReportPath"/>
  <!--远程Tomcat服务的ip地址-->
  <property name="server_ip" value="172.18.2.68"/>
  <!--前面配置的远程Tomcat服务打开的端口，要跟上面配置的一样-->
  <property name="server_port" value="6300"/>
  <!--本地源代码路径-->
  <property name="checkOrderSrcpath" value="C:/Users/xusj/Git/company/report-collection-java/report-collection-app/src/main/java" />
  <!--本地.class文件路径-->
  <property name="checkOrderClasspath" value="C:/Users/xusj/Git/company/report-collection-java/report-collection-app/target/classes" />

  <!--让ant知道去哪儿找Jacoco-->
  <taskdef uri="antlib:org.jacoco.ant" resource="org/jacoco/ant/antlib.xml">
      <classpath path="${jacocoantPath}" />
  </taskdef>

  <!--dump任务:
      根据前面配置的ip地址，和端口号，
      访问目标Tomcat服务，并生成.exec文件。-->
  <target name="dump">
      <jacoco:dump address="${server_ip}" reset="false" destfile="${jacocoexecPath}" port="${server_port}" append="true"/>
  </target>

  <!--jacoco任务:
      根据前面配置的源代码路径和.class文件路径，
      根据dump后，生成的.exec文件，生成最终的html覆盖率报告。-->
  <target name="report">
      <delete dir="${reportfolderPath}" />
      <mkdir dir="${reportfolderPath}" />

      <jacoco:report>
          <executiondata>
              <file file="${jacocoexecPath}" />
          </executiondata>

          <structure name="JaCoCo Report">
              <group name="Check Order related">           
                  <classfiles>
                      <fileset dir="${checkOrderClasspath}" />
                  </classfiles>
                  <sourcefiles encoding="gbk">
                      <fileset dir="${checkOrderSrcpath}" />
                  </sourcefiles>
              </group>
          </structure>

          <html destdir="${reportfolderPath}" encoding="utf-8" />         
      </jacoco:report>
  </target>
</project>
```


##### 生成报表
进入build.xml目录，执行命令
```bash
    ant dump;
    
    ant report
```